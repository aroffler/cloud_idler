# cloud_idler

> Idling game for the cloud initiated.

# TODO



# How to Cloud

### **[We have a trello board!](https://trello.com/b/l9k08mxV/cloud-idler)**

Uses the [IGM(Idle Game Maker)](http://orteil.dashnet.org/igm/) engine created by [@orteil](http://orteil.dashnet.org) 

Help/how-to is [**HERE**](http://orteil.dashnet.org/igm/help.html)

this idle games url is at: 
> http://orteil.dashnet.org/igm/?g=https://gitlab.com/aroffler/cloud_idler/raw/master/cloudidler.txt

art and content CDN is hosted via:
> a [Miroware Pipe](https://miroware.io/users/5d4f2e27927ab206ecfaefef/pipe/#5d4f3ba2927ab206ecfaeffb)

We are using gitlab raw format to host the text files needed to create the environments.
>**WHY?**: because gitlab creates an ideal environment to develop, test, host, and eventually automate the process of the game file structure.

`cloudidler.txt` is the main config file
`css.txt` is the css and format config file

SpriteSheets are being hosted on the Miroware pipe CDN and are built locally by TexturePacker.
>**infrastructureiconsprite.png** handles all infrastructure(building) icons.

>**resourceiconsprite.png** handles all resource icons (i.e the BIT).

>**upgradeiconsprite.png** handles all the upgrade icons.

## Testing?
Testing is easy...
Create a branch, and simply append the new raw file endpoints to `http://orteil.dashnet.org/igm/?g=`

you can work off your test branch long as you need, before doing a PR to the master branch (go-live for prod game files)