
Let's make a game!
    name:Cloud Idler
    by:Espresso
    desc:This is a game for the devops in all of us<//>You too can make servers and automate all the processess!<//><#DE8A35><T>Attributions</T></#><//>Can be found at:<//><i>https://gitlab.com/aroffler/cloud_idler/blob/master/Attributions.md</i>
    created:08/10/2019
    updated:08/11/2019
    version:1.2

Settings
    background:https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/ServerBackground.jpg
    building cost increase:115%
    building cost refund:50%
    spritesheet:infrastructureicons, 48 by 48, https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/infrastructureiconsprite.png
    spritesheet:resourceicons, 48 by 48, https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/resourceiconsprite.png
    spritesheet:upgradeicons, 48 by 48, https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/upgradesiconsprite.png
    stylesheet:https://gitlab.com/aroffler/cloud_idler/raw/master/css.txt


Buttons
    *cloudButton
        name:Generate some Bits!
        desc:Click the cloud to create bits, use bits to buy resources and upgrades for your cloud infrastructure!
        on click:anim icon wobble
        on click:yield 1 bit
        on click:if (have goldenHacks and chance(1%)) yield 1 goldenbit
        //TODO: needs better name for goldenClick
        icon:https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud.png
        no text
        class:bigButton hasFlares
        icon class:shadowed
        tooltip origin:bottom
        tooltip class:red

Layout
    *main
        contains:res, buttons
    *res
        contains:Resources
        class:fullWidth
    *buttons
        contains:Buttons
    *buildings
        contains:BulkDisplay, Buildings
        header:Infrastructure
        class:infrastructure
        tooltip origin:right
    *upgrades
        contains:Upgrades
        header:Upgrades
        class:upgrades
        tooltip origin:top
        tooltip class:tooltip-upgrades
        
        
Resources
    *bit|bits
        name:bit|bits
        desc:These are your bits. You can use them to purchase servers and infrastructure for your cloud. Your goal is to impress management with your cloud skills, and just maybe get that sweet, sweet bonus...maybe...!
        icon:resourceicons[1,0]
        class:noBackground
        show earned

    *goldenbit|goldenbits
            name:Golden bit|Golden bits
            desc:These shiny databits are incredibly rare, terribly precious and important to enlarging your reputation with the CIO!
            icon:resourceicons[0,0]
            class:noBackground
            hidden when 0



Buildings
//Infrastructure
    *TEMPLATE
        on click:anim glow

    //Servers
    *MotherBoard|MotherBoards
        name:Mother Boards|Mother Boards
        desc:Bottom of the server stack, yet without it, these databits are going nowhere fast!
        icon:infrastructureicons[0,0]
        cost:100 bits
        on tick:yield 0.5 bit
        class:infrastructure-button
        unlocked

    *cpu|cpus
        name:CPU|CPUs
        desc:CPUS are the backbone of a well built server, without CPUS, you simply cannot cloud.
        icon:infrastructureicons[1,0]
        cost:200 bits
        on tick:yield 20 bits
        class:infrastructure-button
        req:200 bits:earned

    *rammodule|rammodules
        name:RAM Module|RAM Modules
        desc:The more memory the better right? The DBA down the hall thinks so!
        icon:infrastructureicons[2,0]
        cost:600 bits
        on tick:yield 60 bits
        class:infrastructure-button
        req:500 bits:earned

    *graphicprocessor|graphicprocessors
        name:Graphics Processor|Graphics Processors
        desc:Add more GPUS...Your Bitcoin miner customers will thank you...
        icon:infrastructureicons[3,0]
        cost:2000 bits
        on tick:yield 200 bits
        class:infrastructure-button
        req:1000 bits:earned

    *storagearray|storagearrays
        name:Storage Array|Storage Arrays
        desc:This storage might handle all the cat photos of the internet.
        icon:infrastructureicons[4,0]
        cost:6000 bits
        on tick:yield 600 bits
        class:infrastructure-button
        req:3000 bits:earned
    
    *supportdesk|supportdesks
        name:Support Desk|Support Desks
        desc:The front line of the datacenter.
        icon:infrastructureicons[5,0]
        cost:12000 bits
        on tick:yield 1200 bits
        class:infrastructure-button
        req:8000 bits:earned

    *Engineers|Engineers
        name:Engineer|Engineers
        desc:Solving problems you didn't know you had, in ways you cannot understand.
        icon:infrastructureicons[6,0]
        cost:50000 bits
        on tick:yield 2500 bits
        class:infrastructure-button
        req:25000 bits:earned

    *developer|developers
        name:Developer|Developers
        desc:This strange, and elusive creature is rarely seen outside their favorite IDE(lair). Consider yourself lucky!
        icon:infrastructureicons[7,0]
        cost:100000 bits
        on tick:yield 5000 bits
        class:infrastructure-button
        req:15000 bits:earned

    *automation|automations
        name:Automation|Automations
        desc:Automate literally <b>ALL</b> the things!
        icon:infrastructureicons[8,0]
        cost:200000 bits
        on tick:yield 10000 bits
        class:infrastructure-button
        req:25000 bits:earned

    *integration|integrations
        name:Integration|Integrations
        desc:Building pipelines that would make even Mario throw his linkedin profile at you
        icon:infrastructureicons[9,0]
        cost:300000 bits
        on tick:yield 20000 bits
        class:infrastructure-button
        req:50000 bits:earned
    
    *api|apis
        name:API|APIs
        desc: need desc
        icon:infrastructureicons[11,0]
        cost:1000000 bits
        on tick:yield 500,000 bits
        class:infrastructure-button
        req:500000 bits:earned




Upgrades
    *TEMPLATE
        on click:anim glow

    //goldenbit upgrades
    *goldenHacks
        name:Golden Hack
        desc:The delicate art of hacking the golden bits from secure servers.<//><b>Effect:</b><.>1% chance of gaining 1 golden hacked bit per bit click
        icon:https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/goldendatabit.png
        cost:1 goldenbit
        req:1 goldenbit:earned

    //DataCenter Upgrades
    *ramUpgrade1
        name:Dual Memory Channels
        desc:Double your speed for double the cost!<//><b>Effect:</b><.>Memory production x2<//><.>Happy DBA
        icon:upgradeicons[0,0]
        no text
        cost:500 bits
        passive:multiply yield of rammodule by 2
        class:upgrades-button
        req:(rammodules>=10)

    *ramUpgrade2
        name:32GB Capacity Server Slots
        desc:These bad boys will hold at least half your memory. <i>Slaps memory bank</i><//><b>Effect:</b><.>Memory production x2
        icon:upgradeicons[0,1]
        no text
        cost:20000 bits
        passive:multiply yield of rammodule by 2
        class:upgrades-button
        req:(rammodules>=25 and have ramUpgrade1)
    

    //CloudCreator upgrades

