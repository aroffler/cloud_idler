# Attributions
- Background photo by panumas nikhomkhai from [Pexels.com](http://wwww.Pexels.com)

- Icon art used with permission from [iconninja.com](http://www.iconninja.com)

- CloudIcon used by permission License: [Linkware](http://www.pelfusion.com)

- Button background from <a href="https://www.freepik.com/free-photos-vectors/background">Background vector created by starline - www.freepik.com</a>

- Header background from <a href="https://www.freepik.com/free-photos-vectors/background">Background vector created by starline - www.freepik.com</a>

- Icon Art used by permission from <a href="http://www.iconarchive.com">IconArchive</a>
    > ##### artists include:
    >
    > Iynque: http://www.iconarchive.com/artist/iynque.html